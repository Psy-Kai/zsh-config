# zsh-config

My zsh config on my majaro system.

## Requirements:
*  zsh
*  oh-my-zsh.git
*  zsh-autosuggestions
*  zsh-syntax-highlighting
*  zsh-theme-powerlevel10k-git
*  ttf-meslo-nerd-font-powerlevel10k

### Terminal Tools
*  [cd-replacement](https://github.com/ajeetdsouza/zoxide)
*  [fuzzy finder](https://github.com/junegunn/fzf)